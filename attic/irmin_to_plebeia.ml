(* A tool to copy Irmin contexts to Plebeia contexts not via Tezos protocol.

   $ dune exec ./irmin_to_plebeia.exe -- $HOME/.tezos-node

   This copies the contents of $HOME/.tezos-node/context
   to $HOME/.tezos-node/plebeia.context.

   - Target blocks are obtained from the "store", since Tezos does not
     record its heads in Irmin context.
   - Each diff beween Irmin contexts is applied to Plebeia.
   - A directory copy is not replicated as is, therefore inefficient in size.
   - Correctness is NOT checked: the final Plebeia hash MUST be compared with
     the hash obtained via the protocol.

   This tool is very slow, probably the main bottleneck is obtaining diffs
   in Irmin.  You can bootstrap faster by running a Tezos node with Plebeia
   from the genesiis.
*)

open Icontext
module Path = Irmin.Path.String_list
module Metadata = Irmin.Metadata.None

module Hash : sig
  include Irmin.Hash.S

  val to_context_hash : t -> Context_hash.t

  val of_context_hash : Context_hash.t -> t
end = struct
  module H = Digestif.Make_BLAKE2B (struct
    let digest_size = 32
  end)

  type t = H.t

  let of_context_hash s = H.of_raw_string (Context_hash.to_string s)

  let to_context_hash h = Context_hash.of_string_exn (H.to_raw_string h)

  let pp ppf t = Context_hash.pp ppf (to_context_hash t)

  let of_string x =
    match Context_hash.of_b58check x with
    | Ok x ->
        Ok (of_context_hash x)
    | Error err ->
        Error
          (`Msg
            (Format.asprintf
               "Failed to read b58check_encoding data: %a"
               Error_monad.pp_print_error
               err))

  let short_hash t = Irmin.Type.(short_hash string (H.to_raw_string t))

  let t : t Irmin.Type.t =
    Irmin.Type.map
      ~cli:(pp, of_string)
      Irmin.Type.(string_of (`Fixed H.digest_size))
      ~short_hash
      H.of_raw_string
      H.to_raw_string

  let hash_size = H.digest_size

  let hash = H.digesti_string
end

module Node = struct
  module M = Irmin.Private.Node.Make (Hash) (Path) (Metadata)

  module V1 = struct
    module Hash = Irmin.Hash.V1 (Hash)

    type kind = [`Node | `Contents of Metadata.t]

    type entry = {kind : kind; name : M.step; node : Hash.t}

    (* Irmin 1.4 uses int64 to store string lengths *)
    let step_t =
      let pre_hash = Irmin.Type.(pre_hash (string_of `Int64)) in
      Irmin.Type.like M.step_t ~pre_hash

    let metadata_t =
      let some = "\255\000\000\000\000\000\000\000" in
      let none = "\000\000\000\000\000\000\000\000" in
      Irmin.Type.(map (string_of (`Fixed 8)))
        (fun s ->
          match s.[0] with
          | '\255' ->
              None
          | '\000' ->
              Some ()
          | _ ->
              assert false)
        (function Some _ -> some | None -> none)

    (* Irmin 1.4 uses int64 to store list lengths *)
    let entry_t : entry Irmin.Type.t =
      let open Irmin.Type in
      record "Tree.entry" (fun kind name node ->
          let kind = match kind with None -> `Node | Some m -> `Contents m in
          {kind; name; node})
      |+ field "kind" metadata_t (function
             | {kind = `Node; _} ->
                 None
             | {kind = `Contents m; _} ->
                 Some m)
      |+ field "name" step_t (fun {name; _} -> name)
      |+ field "node" Hash.t (fun {node; _} -> node)
      |> sealr

    let entries_t : entry list Irmin.Type.t =
      Irmin.Type.(list ~len:`Int64 entry_t)

    let import_entry (s, v) =
      match v with
      | `Node h ->
          {name = s; kind = `Node; node = h}
      | `Contents (h, m) ->
          {name = s; kind = `Contents m; node = h}

    let import t = Stdlib.List.map import_entry (M.list t)

    let pre_hash entries = Irmin.Type.pre_hash entries_t entries
  end

  include M

  let pre_hash_v1 x = V1.pre_hash (V1.import x)

  let t = Irmin.Type.(like t ~pre_hash:pre_hash_v1)
end

module Commit = struct
  module M = Irmin.Private.Commit.Make (Hash)
  module V1 = Irmin.Private.Commit.V1 (M)
  include M

  let pre_hash_v1 t = Irmin.Type.pre_hash V1.t (V1.import t)

  let t = Irmin.Type.like t ~pre_hash:pre_hash_v1
end

module Contents = struct
  type t = string

  let pre_hash_v1 x =
    let ty = Irmin.Type.(pair (string_of `Int64) unit) in
    Irmin.Type.(pre_hash ty) (x, ())

  let t = Irmin.Type.(like ~pre_hash:pre_hash_v1 string)

  let merge = Irmin.Merge.(idempotent (Irmin.Type.option t))
end

module Conf = struct
  let entries = 32

  let stable_hash = 256
end

module IStore =
  Irmin_pack.Make_ext (Conf) (Irmin.Metadata.None) (Contents)
    (Irmin.Path.String_list)
    (Irmin.Branch.String)
    (Hash)
    (Node)
    (Commit)

type index = {
  path : string;
  repo : IStore.Repo.t;
  patch_context : (context -> context tzresult Lwt.t) option;
}

(* from node_config_file.ml *)
let genesis =
  {
    Genesis.time = Time.Protocol.of_notation_exn "2018-06-30T16:07:32Z";
    block =
      Block_hash.of_b58check_exn
        "BLockGenesisGenesisGenesisGenesisGenesisf79b5d1CoW2";
    protocol =
      Protocol_hash.of_b58check_exn
        "Ps9mPmXaRzmzk35gbAYNCAw6UXdE2qoABTHbN2oEEc1qM7CwT9P";
  }

module Set = Set.Make (struct
  type t = Int32.t * Block_hash.t * Context_hash.t

  let compare (l1, bh1, _ch1) (l2, bh2, _ch2) =
    match Int32.compare l1 l2 with 0 -> Block_hash.compare bh1 bh2 | x -> x
end)

let get_blocks data_dir =
  let store_root = Filename.concat data_dir "store" in
  Format.eprintf "%s@." store_root ;
  Store.init (* read_only fails *) ~mapsize:40_960_000_000L store_root
  >>=? fun gstore ->
  Format.eprintf "got store@." ;
  Store.Chain.list gstore
  >>= fun chains ->
  Format.eprintf "%d chains@." (Stdlib.List.length chains) ;
  let cntr = ref 0 in
  Lwt_list.fold_left_s
    (fun bhset cid ->
      let chain_store = Store.Chain.get gstore cid in
      let chain_data_store = Store.Chain_data.get chain_store in
      Store.Chain_data.Known_heads.read_all chain_data_store
      >>= fun head_bhashes ->
      Format.eprintf
        "%a: %d known heads@."
        Chain_id.pp
        cid
        (Block_hash.Set.cardinal head_bhashes) ;
      let block_store = Store.Block.get chain_store in
      Lwt_list.fold_left_s
        (fun bhset head_bhash ->
          let rec f bhash bhset =
            Store.Block.Contents.read (block_store, bhash)
            >>= function
            | Error _ ->
                assert false
            | Ok contents ->
                let level = contents.header.shell.level in
                let chash = contents.header.shell.context in
                if Set.mem (level, bhash, chash) bhset then Lwt.return bhset
                else
                  let bhset = Set.add (level, bhash, chash) bhset in
                  incr cntr ;
                  if !cntr mod 10000 = 0 then
                    Format.eprintf "%d bhashes@." !cntr ;
                  f contents.header.shell.predecessor bhset
          in
          f head_bhash bhset)
        bhset
        (Block_hash.Set.elements head_bhashes))
    Set.empty
    chains
  >|= fun bhset -> Ok bhset

let get_irmin_diff repo bhash chash =
  IStore.Commit.of_hash repo (Hash.of_context_hash chash)
  >>= function
  | None ->
      Format.eprintf "%a: checkout failed@." Context_hash.pp chash ;
      assert false
  | Some commit ->
      let parent_chash =
        match IStore.Commit.parents commit with
        | [] when bhash = genesis.block ->
            None
        | [] ->
            assert false
        | _ :: _ :: _ ->
            assert false
        | [parent_chash] ->
            Some parent_chash
      in
      ( match parent_chash with
      | None ->
          Lwt.return IStore.Tree.empty
      | Some parent_chash -> (
          IStore.Commit.of_hash repo parent_chash
          >|= function
          | None ->
              Format.eprintf "%a: checkout failed@." Context_hash.pp chash ;
              assert false
          | Some commit ->
              IStore.Commit.tree commit ) )
      >>= fun parent_tree ->
      let tree = IStore.Commit.tree commit in
      (* Even if there is no diff, Irmin's context hashes are different,
         since its hash is computed from:

         * time
         * author (always "Tezos")
         * message (always "")
         * parent hashes (at most 1)
         * the hash of the tree
      *)
      IStore.Tree.diff parent_tree tree
      >|= fun diff ->
      (Option.map Hash.to_context_hash parent_chash, diff, commit)

let pcontext_apply_diff pctxt diff =
  let apply pctxt = function
    | (k, `Updated (_, (data, ()))) | (k, `Added (data, ())) -> (
      try Pcontext.raw_set pctxt k (Bytes.of_string data)
      with e ->
        Format.eprintf "add %s@." (String.concat "/" k) ;
        raise e )
    | (k, `Removed _) -> (
      try Pcontext.raw_del pctxt k
      with e ->
        Format.eprintf "del %s@." (String.concat "/" k) ;
        raise e )
  in
  Lwt_list.fold_left_s apply pctxt diff

let test data_dir =
  get_blocks data_dir
  >>=? fun bhset ->
  let context_root = Filename.concat data_dir "context" in
  IStore.Repo.v (Irmin_pack.config ~readonly:true context_root)
  >>= fun repo ->
  (* let _v = {path = root; repo; patch_context= None} in *)

  (* XXX This creates data_dir/plebeia.context.  Very confusing *)
  Pcontext.init ~readonly:false context_root
  >>= fun pi ->
  let elems = Set.elements bhset in
  Lwt_list.fold_left_s
    (fun () (level, bhash, chash) ->
      Format.eprintf "%ld %a@." level Block_hash.pp bhash ;
      (* IStore.Repo.heads and branches are not working.
         That's why we asked the store for block hashes *)
      get_irmin_diff repo bhash chash
      >>= fun (parent_chash, diff, commit) ->
      let info = IStore.Commit.info commit in
      let time = Time.Protocol.of_seconds @@ Irmin.Info.date info in
      let message = Irmin.Info.message info in
      Format.eprintf
        "checkout %a %d diffs@."
        Context_hash.pp
        chash
        (Stdlib.List.length diff) ;
      ( match parent_chash with
      | None ->
          Lwt.return @@ Pcontext.empty_context pi
      | Some parent_chash -> (
          Pcontext.checkout pi parent_chash
          >>= function None -> assert false | Some pctxt -> Lwt.return pctxt )
      )
      >>= fun pctxt ->
      pcontext_apply_diff pctxt diff
      >>= fun pctxt ->
      Pcontext.commit ~hash_override:(Some chash) ~level ~time ~message pctxt
      >>= fun _chash ->
      Format.eprintf "commit %a@." Context_hash.pp chash ;
      Lwt.return ())
    ()
    elems
  >>= fun () -> return ()

let () =
  Lwt_main.run
    ( test Sys.argv.(1)
    >>= function Ok () -> Lwt.return () | Error _ -> assert false )
