open Path_summarize_lib
open Utils

type leaf =
  | Samples of StringSet.t
  | Statistics of { min: int; avg: float; max: int }

type t =
  | Digit of  { len: int; n: int; nsubs: int; sub: t }
  | Hex of { len: int; n: int; nsubs: int; sub: t }
  | Leaf of leaf
  | Name of { n: int; subs: (string * (int * t)) list }

let stat_huge_leaves t =
  let rec f = function
    | Digit d -> Digit { d with sub= f d.sub }
    | Hex d -> Hex { d with sub= f d.sub }
    | Name r -> Name { r with subs= List.map (fun (n, (nsubs, t)) -> (n, (nsubs, f t))) r.subs }
    | Leaf (Samples s) ->
        let sz = StringSet.cardinal s in
        if sz > 1000 then
          let min, max, sum = StringSet.fold (fun s (min, max, sum) ->
              let len = String.length s in
              Stdlib.min len min, Stdlib.max len max, sum + len)
              s (1000000,0,0)
          in
          let avg = float sum /. float sz in
          Leaf (Statistics { min; avg; max })
        else Leaf (Samples s)
    | Leaf stat -> Leaf stat
  in
  f t

let string_of_pat = function
  | `Name n -> Printf.sprintf "%s" n
  | `H n -> Printf.sprintf "<H%d>" n
  | `D -> Printf.sprintf "<D>"

let kind rev_whole n =
  let len = String.length n in
  let cs = List.of_seq & String.to_seq n in
  let is_digit = function
    | '0' .. '9' -> true
    | _ -> false
  in
  let is_hex = function
    | '0' .. '9' | 'a' .. 'f' -> true
    | _ -> false
  in
  if List.for_all is_digit cs then
    `Digit len
  else
    match cs with
    | '-'::cs when
        Format.eprintf "warning: strange int index %s@." (string_of_key (List.rev rev_whole));
        List.for_all is_digit cs -> `Digit 0 (* cannot mix with hex *)
    | _ ->
        if len mod 2 = 0 && List.for_all is_hex cs then `Hex len
        else `Name

let merge_kind = function
  | `Name, `Name -> `Name
  | `Digit len, `Digit len' -> `Digit (if len = len' then len else 0)
  | `Hex len, `Hex len' when len = len' -> `Hex len
  | `Digit len, `Hex len'
  | `Hex len, `Digit len' when len = len' -> `Hex len
  | _, _ -> `Name

let rec lines_of_d =
  let indent s ls =
    match ls with
    | [] -> assert false
    | l::ls ->
        let i = String.make (String.length s) ' ' in
        (s ^ l) :: List.map (fun l -> i ^ l) ls
  in
  function
  | Leaf (Samples s) ->
      let n = StringSet.cardinal s in
      let min_, max_ = StringSet.fold (fun s (min_, max_) -> let len = String.length s in (min min_ len, max max_ len)) s (1000000,0) in
      [Printf.sprintf "LEAF(samples=%d,min=%d,max=%d)" n min_ max_]
  | Leaf (Statistics {min; avg; max}) ->
      [Printf.sprintf "LEAF(min=%d,avg=%.2f,max=%d)" min avg max]
  | Name { n; subs= nds } ->
      List.concat & List.map (fun (s,(m, d)) -> indent (Printf.sprintf "%s(%.2f)" s (float m /. float n) ^ "/") & lines_of_d d) nds
  | Digit { len=_n; n= ndirs; nsubs= nsubdirs; sub= d } ->
      indent (Printf.sprintf "D(%.2f)/" (float nsubdirs /. float ndirs)) & lines_of_d d
  | Hex { len=n; n= ndirs; nsubs= nsubdirs; sub= d } ->
      indent (Printf.sprintf "H%d(%.2f)/" n (float nsubdirs /. float ndirs)) & lines_of_d d

let string_of_d d = String.concat "\n" (lines_of_d d)

let rec merge_ds ds =
  match ds with
  | [] -> assert false
  | d::ds ->
      List.fold_left (fun m d ->
          match m, d with
          | Leaf (Samples vs), Leaf (Samples vs') -> Leaf (Samples (StringSet.union vs vs'))
          | Leaf _, Leaf _ -> assert false
          | Name { n; subs= nds }, Name { n=n'; subs= nds' } ->
              let ns = List.sort_uniq compare & List.map fst (List.rev_append nds nds') in
              Name { n= n + n'
                   ; subs= List.map (fun n ->
                         n,
                         match List.assoc_opt n nds, List.assoc_opt n nds' with
                         | None, None -> assert false
                         | Some d, None -> d
                         | None, Some d -> d
                         | Some (n, d), Some (n', d') -> n + n', merge_ds [d; d'])
                         ns
                   }
          | Digit { len= n; n= ndirs; nsubs= nsubdirs; sub= d }, Digit { len= n'; n= ndirs'; nsubs= nsubdirs'; sub= d' } ->
              let n = if n = n' then n else 0 in
              Digit { len= n; n= ndirs + ndirs'; nsubs= nsubdirs + nsubdirs'; sub= merge_ds [d; d'] }
          | Hex { len= n; n= ndirs; nsubs= nsubdirs; sub= d }, Hex { len= n'; n= ndirs'; nsubs= nsubdirs'; sub= d' } when n = n' ->
              Hex { len= n; n= ndirs + ndirs'; nsubs= nsubdirs + nsubdirs'; sub= merge_ds [d; d'] }
          | Digit { len= n; n= ndirs; nsubs= nsubdirs; sub= d }, Hex { len= n'; n= ndirs'; nsubs=  nsubdirs'; sub=  d' }
          | Hex { len= n; n= ndirs; nsubs= nsubdirs; sub= d }, Digit { len= n'; n= ndirs'; nsubs= nsubdirs'; sub= d' } when n = n' ->
              Hex { len= n; n= ndirs + ndirs'; nsubs= nsubdirs + nsubdirs'; sub= merge_ds [d; d'] }
          | Hex { len= n; n= ndirs; nsubs= nsubdirs; sub= d }, Hex { len= n'; n= ndirs'; nsubs= nsubdirs'; sub= d' } when n = n' ->
              Hex { len= n; n= ndirs + ndirs'; nsubs= nsubdirs + nsubdirs'; sub= merge_ds [d; d'] }
          | x1, x2 ->
              Format.eprintf "%s@. and@.%s@." (string_of_d x1) (string_of_d x2);
              assert false
        ) d ds

let merge_ds ds =
  try merge_ds ds with
  | e ->
      List.iter (fun d -> Format.eprintf "%s@.@." (string_of_d d)) ds;
      raise e

let summarize rt ofile =
  let rec f rev_cur rt = match rt with
    | Raw_tree.Leaf s -> Leaf (Samples (StringSet.singleton s))
    | Dir map ->
        let ds =
          StringMap.fold (fun s rt st ->
              (s, (f (s::rev_cur) rt))::st) map []
        in
        let k =
          let ss = List.map fst ds in
          match ss with
          | [] -> assert false
          | s::ss ->
              List.fold_left (fun k s ->
                  let k' = kind (s::rev_cur) s in
                  merge_kind (k, k')) (kind (s::rev_cur) s) ss
        in
        match k with
        | `Name -> Name { n=1; subs= List.map (fun (n,d) -> (n, (1, d))) ds }
        | `Digit n -> Digit { len= n; n= 1; nsubs= List.length ds; sub= merge_ds (List.map snd ds) }
        | `Hex n -> Hex { len= n; n= 1; nsubs= List.length ds; sub= merge_ds (List.map snd ds) }
  in
  let d = f [] rt in
  let d = stat_huge_leaves d in
  List.iter prerr_endline & lines_of_d d;
  let oc = open_out ofile in
  output_value oc d;
  close_out oc

let () =
  let argv = List.tl & Array.to_list Sys.argv in
  match argv with
  | "summarize" :: file :: ofile :: [] ->
      let ic = open_in file in
      let rt : Raw_tree.t = input_value ic in
      close_in ic;
      ignore @@ summarize rt ofile
  | _ ->
      assert false
