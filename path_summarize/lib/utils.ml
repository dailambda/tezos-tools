module Misc = struct
  let ( & ) = ( @@ )

  let ( ^/ ) = Filename.concat

  let failwithf fmt = Printf.ksprintf Stdlib.failwith fmt
end

include Misc

module Format = struct
  include Format

  let string = pp_print_string

  let rec list (sep : (unit, formatter, unit) format) p ppf = function
    | [] ->
        ()
    | [x] ->
        p ppf x
    | x :: xs ->
        fprintf
          ppf
          "@[%a@]%t%a"
          p
          x
          (fun ppf -> fprintf ppf sep)
          (list sep p)
          xs
end

module String = struct
  include String

  let find_char p s pos =
    let len = String.length s in
    let rec f pos =
      if len <= pos then None
      else if p @@ String.unsafe_get s pos then Some pos
      else f (pos + 1)
    in
    f pos

  let split_by_char p s =
    let len = String.length s in
    let rec f rev_list start pos =
      match find_char p s pos with
      | None ->
          List.rev_map (fun (a, b) -> String.sub s a b)
          @@ ((start, len - start) :: rev_list)
      | Some pos' ->
          f ((start, pos' - start) :: rev_list) (pos' + 1) (pos' + 1)
    in
    f [] 0 0

  let () =
    assert (
      split_by_char (function '/' -> true | _ -> false) "/1/23//456/"
      = [""; "1"; "23"; ""; "456"; ""] ) ;
    assert (split_by_char (function '/' -> true | _ -> false) "/" = [""; ""]
    ) ;
    assert (split_by_char (function '/' -> true | _ -> false) "" = [""])

  let for_all f s =
    let len = String.length s in
    let rec aux = function
      | -1 ->
          true
      | i ->
          let c = String.unsafe_get s i in
          if f c then aux (i - 1) else false
    in
    aux (len - 1)
end

module Key = struct
  type t = string list

  let pp ppf k =
    Format.string ppf (String.escaped ("/" ^ String.concat "/" k))

  let parse s =
    match String.split_by_char (function '/' -> true | _ -> false) s with
    | "" :: xs ->
        List.filter (function "" -> false | _ -> true) xs
    | _ ->
        assert false
end

let string_of_key = String.concat "/"

module StringMap = Map.Make(String)
module StringSet = Set.Make(String)
