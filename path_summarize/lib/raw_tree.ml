open Utils

type t =
  | Leaf of string
  | Dir of t StringMap.t

let hashcons hc s =
  match StringSet.find_opt s hc with
  | Some s -> hc, s
  | None ->
      StringSet.add s hc, s

let hashcons_empty = StringSet.empty

let add hc k bytes rt =
  let hc, s = hashcons hc & Bytes.to_string bytes in
  let hc, k =
    List.fold_right (fun n (hc,k) ->
        let hc, n = hashcons hc n in
        hc, n::k) k (hc,[])
  in
  let rec f k rt =
    match k, rt with
    | [], _ -> assert false
    | [n], Dir map -> Dir (StringMap.add n (Leaf s) map)
    | n::k, Dir map ->
        let rt =
          match StringMap.find_opt n map with
          | None -> Dir StringMap.empty
          | Some rt -> rt
        in
        Dir (StringMap.add n (f k rt) map)
    | _::_, Leaf _ -> assert false
  in
  hc, f k rt
