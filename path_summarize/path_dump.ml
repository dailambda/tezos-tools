open Tezos_shell
open Tezos_storage
open Tezos_base__TzPervasives
open Lwt

open Path_summarize_lib
open Utils

module Tezos = struct
  let check_data_mdb dir =
    if not @@ Sys.file_exists (dir ^/ "data.mdb") then
      failwithf "%s has no data.mdb" dir

  let check_store_pack dir =
    if not @@ Sys.file_exists (dir ^/ "store.pack") then
      failwithf "%s has no store.pack" dir

  let with_store dir f =
    check_data_mdb dir ;
    Store.init dir
    >>= function
    | Ok store ->
        Lwt.finalize
          (fun () -> f store)
          (fun () -> Store.close store ; Lwt.return ())
    | Error err ->
        Format.kasprintf
          Stdlib.failwith
          "@[Cannot initialize store:@ %a@]"
          pp_print_error
          err

  let with_context dir f =
    check_store_pack dir ;
    Context.init dir >>= fun index -> f index
end

include Tezos

let get_context_hash gstore block_hash_opt =
  Store.Chain.list gstore
  >>= fun chain_ids ->
  let chain_id = match chain_ids with [cid] -> cid | _ -> assert false in
  Format.eprintf
    "chain_id= %a %a@."
    Chain_id.pp
    chain_id
    Hex.pp
    (Chain_id.to_hex chain_id) ;
  let chain = Store.Chain.get gstore chain_id in
  prerr_endline "chain store got" ;
  let chain_data = Store.Chain_data.get chain in
  (* The store knows the head.  The context does NOT. *)
  ( match block_hash_opt with
  | Some _ ->
      Lwt.return block_hash_opt
  | None ->
      Store.Chain_data.Current_head.read_opt chain_data )
  >>= function
  | None ->
      assert false
  | Some block_hash -> (
      Format.eprintf "block hash: %a@." Block_hash.pp block_hash ;
      (* Block hash is NOT the key used in the context.
         We need the context hash instead, which is stored in the block.
      *)
      let block = Store.Block.get chain in
      Store.Block.Contents.read_opt (block, block_hash)
      >>= function
      | None ->
          assert false
      | Some contents ->
          Format.eprintf
            "head context hash: %a@."
            Context_hash.pp
            contents.Store.Block.context ;
          Lwt.return contents.Store.Block.context )

let bytes_pp_hex ppf v =
  let (`Hex s) = Hex.of_bytes v in
  Format.fprintf ppf "%s" s

let context_cat context key =
  Context.find context key
  >>= function
  | None ->
      Lwt.return None
  | Some v ->
      Format.eprintf "data: %S@." (Bytes.to_string v) ;
      Format.eprintf "data in hex: %a@." bytes_pp_hex v ;
      ( try
          Format.eprintf
            "data in b58check: %s@."
            Block_hash.(to_b58check @@ of_bytes_exn v)
        with _ -> () ) ;
      Lwt.return (Some v)

let scrape context_hash index =
  (* let dirs = Hashtbl.create 1000 in *)
  let t0 = Unix.gettimeofday () in
  Context.checkout index context_hash
  >>= function
  | None ->
      assert false
  | Some context ->
      Context.fold context [] ~init:(Raw_tree.hashcons_empty, Raw_tree.Dir (StringMap.empty), 0) ~f:(fun k tree (hc,rt,n) ->
          match Context.Tree.kind tree with
          | `Tree ->
              Lwt.return (hc, rt, n)
          | `Value ->
              Context.Tree.find tree [] >>= function
              | None -> assert false
              | Some bytes ->
                  let hc, rt = Raw_tree.add hc k bytes rt in
                  let n = n + 1 in
                  if n mod 100000 = 0 then begin
                    let t1 = Unix.gettimeofday () in
                    let eta = ((t1 -. t0) /. float n) *. (7000000. -. float n) in
                    Format.eprintf "%d/about 7000000 %.2fmin ETA %.2fmin@." n ((t1 -. t0) /. 60.0) (eta /. 60.0)
                  end;
                  Lwt.return (hc, rt, n)) >>= fun (_hc, rt, _) ->
      Lwt.return rt

let scrape dir block_hash_opt ofile =
  with_store (dir ^/ "store")
  & fun gstore ->
  with_context (dir ^/ "context")
  & fun index ->
  get_context_hash gstore block_hash_opt
  >>= fun context_hash -> scrape context_hash index
  >>= fun rt ->
  let oc = open_out ofile in
  output_value oc rt;
  close_out oc;
  Lwt.return ()

let () =
  let argv = Stdlib.List.tl & Array.to_list Sys.argv in
  match argv with
  | "dump" :: dir :: block_hash :: ofile :: [] ->
      let block_hash_opt =
        match block_hash with
        | "head" ->
            None
        | _ ->
            Some (Block_hash.of_string_exn block_hash)
      in
      Lwt_main.run & scrape dir block_hash_opt ofile
  | _ ->
      assert false
