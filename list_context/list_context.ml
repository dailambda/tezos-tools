(* Peek into Tezos' context of the given block hash.

   $ dune exec ./check_context.exe ~/.tezos-node head /
   $ dune exec ./check_context.exe ~/.tezos-node head /version
   $ dune exec ./check_context.exe ~/.tezos-node head /contracts
*)

open Tezos_shell
open Tezos_storage
open Tezos_base__TzPervasives
open Lwt

include struct
  module Misc = struct
    let ( & ) = ( @@ )

    let ( ^/ ) = Filename.concat

    let failwithf fmt = Printf.ksprintf Stdlib.failwith fmt
  end

  open Misc

  module Format = struct
    include Format

    let string = pp_print_string

    let rec list (sep : (unit, formatter, unit) format) p ppf = function
      | [] ->
          ()
      | [x] ->
          p ppf x
      | x :: xs ->
          fprintf
            ppf
            "@[%a@]%t%a"
            p
            x
            (fun ppf -> fprintf ppf sep)
            (list sep p)
            xs
  end

  module String = struct
    include String

    let find_char p s pos =
      let len = String.length s in
      let rec f pos =
        if len <= pos then None
        else if p @@ String.unsafe_get s pos then Some pos
        else f (pos + 1)
      in
      f pos

    let split_by_char p s =
      let len = String.length s in
      let rec f rev_list start pos =
        match find_char p s pos with
        | None ->
            List.rev_map (fun (a, b) -> String.sub s a b)
            @@ ((start, len - start) :: rev_list)
        | Some pos' ->
            f ((start, pos' - start) :: rev_list) (pos' + 1) (pos' + 1)
      in
      f [] 0 0

    let () =
      assert (
        split_by_char (function '/' -> true | _ -> false) "/1/23//456/"
        = [""; "1"; "23"; ""; "456"; ""] ) ;
      assert (split_by_char (function '/' -> true | _ -> false) "/" = [""; ""]
      ) ;
      assert (split_by_char (function '/' -> true | _ -> false) "" = [""])

    let for_all f s =
      let len = String.length s in
      let rec aux = function
        | -1 ->
            true
        | i ->
            let c = String.unsafe_get s i in
            if f c then aux (i - 1) else false
      in
      aux (len - 1)
  end

  module Key = struct
    type t = string list

    let pp ppf k =
      Format.string ppf (String.escaped ("/" ^ String.concat "/" k))

    let parse s =
      match String.split_by_char (function '/' -> true | _ -> false) s with
      | "" :: xs ->
          List.filter (function "" -> false | _ -> true) xs
      | _ ->
          assert false
  end

  let check_data_mdb dir =
    if not @@ Sys.file_exists (dir ^/ "data.mdb") then
      failwithf "%s has no data.mdb" dir

  let check_store_pack dir =
    if not @@ Sys.file_exists (dir ^/ "store.pack") then
      failwithf "%s has no store.pack" dir

  let with_store dir f =
    check_data_mdb dir ;
    Store.init dir
    >>= function
    | Ok store ->
        Lwt.finalize
          (fun () -> f store)
          (fun () -> Store.close store ; Lwt.return ())
    | Error err ->
        Format.kasprintf
          Stdlib.failwith
          "@[Cannot initialize store:@ %a@]"
          pp_print_error
          err

  let with_context dir f =
    check_store_pack dir ;
    Context.init dir >>= fun index -> f index
end

open Misc

let get_context_hash gstore block_hash_opt =
  Store.Chain.list gstore
  >>= fun chain_ids ->
  let chain_id = match chain_ids with [cid] -> cid | _ -> assert false in
  Format.eprintf
    "chain_id= %a %a@."
    Chain_id.pp
    chain_id
    Hex.pp
    (Chain_id.to_hex chain_id) ;
  let chain = Store.Chain.get gstore chain_id in
  prerr_endline "chain store got" ;
  let chain_data = Store.Chain_data.get chain in
  (* The store knows the head.  The context does NOT. *)
  ( match block_hash_opt with
  | Some _ ->
      Lwt.return block_hash_opt
  | None ->
      Store.Chain_data.Current_head.read_opt chain_data )
  >>= function
  | None ->
      assert false
  | Some block_hash -> (
      Format.eprintf "block hash: %a@." Block_hash.pp block_hash ;
      (* Block hash is NOT the key used in the context.
         We need the context hash instead, which is stored in the block.
      *)
      let block = Store.Block.get chain in
      Store.Block.Contents.read_opt (block, block_hash)
      >>= function
      | None ->
          assert false
      | Some contents ->
          Format.eprintf
            "head context hash: %a@."
            Context_hash.pp
            contents.Store.Block.context ;
          Lwt.return contents.Store.Block.context )

let bytes_pp_hex ppf v =
  let (`Hex s) = Hex.of_bytes v in
  Format.fprintf ppf "%s" s

let context_cat context key =
  Context.find context key
  >>= function
  | None ->
      Lwt.return None
  | Some v ->
      Format.eprintf "data: %S@." (Bytes.to_string v) ;
      Format.eprintf "data in hex: %a@." bytes_pp_hex v ;
      ( try
          Format.eprintf
            "data in b58check: %s@."
            Block_hash.(to_b58check @@ of_bytes_exn v)
        with _ -> () ) ;
      Lwt.return (Some v)

let key_pp ppf k = Format.fprintf ppf "/%s" (String.concat "/" k)

let context_ls context_hash index ks =
  Context.checkout index context_hash
  >>= function
  | None ->
      assert false
  | Some context ->
      Context.find_tree context ks >>= function
      | None ->
          Format.eprintf "%a: no such file or directory@." key_pp ks;
          Lwt.return ()
      | Some tree ->
          match Context.Tree.kind tree with
          | `Value ->
              Context.Tree.to_value tree >>= (function
                  | None -> assert false
                  | Some v ->
                      Format.eprintf "%a: file: %d@." key_pp ks (Bytes.length v);
                      Lwt.return ()
                )
          | `Tree ->
              Context.Tree.list tree []
              >>= fun kts ->
              Format.eprintf "%a:@." key_pp ks;
              List.iter (fun (k,_) ->
                  Format.eprintf "  %a@." key_pp [k]) kts;
              Lwt.return ()

let ( & ) = ( @@ )

let t dir block_hash_opt k =
  with_store (dir ^/ "store")
  & fun gstore ->
  with_context (dir ^/ "context")
  & fun index ->
  get_context_hash gstore block_hash_opt
  >>= fun context_hash -> context_ls context_hash index k

let () =
  let argv = Stdlib.List.tl & Array.to_list Sys.argv in
  match argv with
  | dir :: block_hash :: ks ->
      let k =
        match ks with [] -> [] | [k] -> Key.parse k | _ -> assert false
      in
      Format.eprintf "%s@." (String.concat "/" k) ;
      let block_hash_opt =
        match block_hash with
        | "head" ->
            None
        | _ ->
            Some (Block_hash.of_string_exn block_hash)
      in
      Lwt_main.run & t dir block_hash_opt k
  | _ ->
      assert false
